KohlerMarketModule.controller('PreviewController', function ($scope, ApplicationScopes, marketmappingpreviewservice, WebAPIurl, $location, $routeParams) {
    //alert(ApplicationScopes.get('MyActionWidgetController').lblAddNewState);
    //  $scope.ApplicationScopes.
    $('#container').show()
    $('#spinner').hide()
    $scope.lblAddNewState = ApplicationScopes.get('MarketMappingController').lblAddNewState;

    $scope.GetValues = function (fieldName) {
        //if (fieldName.indexOf("dealerInfo") > -1 || fieldName.indexOf("businessSplit") > -1 || fieldName.indexOf("storeArea") > -1) {
        //   // debugger;
        //     //alert(ApplicationScopes.get('MyActionWidgetController')[fieldName.split('.')[0]])
        //    return ApplicationScopes.get('MarketMappingController')[fieldName.split('.')[0]] != undefined ? ApplicationScopes.get('MarketMappingController')[fieldName.split('.')[0]][fieldName.split('.')[1]] : "";
        //}
        return ApplicationScopes.get('MarketMappingController')[fieldName.split('.')[0]] != undefined ? ApplicationScopes.get('MarketMappingController')[fieldName.split('.')[0]][fieldName.split('.')[1]] : "";
        //return ApplicationScopes.get('MarketMappingController')[fieldName] != null ? ApplicationScopes.get('MarketMappingController')[fieldName] : "";
    }

    $scope.SubmitData = function () {
        $('#container').hide()
        $('#spinner').show()

        var fieldValues = ApplicationScopes.get('MarketMappingController');

        console.log('full field values')
        console.log(fieldValues)
        marketmappingpreviewservice.SubmitData(function (results) {
            //debugger;
            //alert("Data Submitted successfully");
            // console.log('redirecting')
            ApplicationScopes.store('stagedData', undefined);
            $location.path("/landing/" + $routeParams.token + "/" + $routeParams.ko);
            alert("Saved Successfully!")

        }, WebAPIurl, fieldValues, $routeParams.ko, $routeParams.prospectId);
    }
});