﻿// Creating a factory from the module

KohlerMarketModule.factory('marketmappingpreviewservice', function ($http) {
    //debugger;
    return {
        SubmitData: function (callback, WebAPIurl, fieldValues, ko, prospectId) {

            var url = WebAPIurl + 'postSurvey';
            console.log("fieldValues")
            console.log(fieldValues.dealerInfo)
            $http({
                method: "POST",
                url: url,
                headers: {'Content-Type': 'application/json'},
                data: {
                    user: ko,
                    prospectId: prospectId,
                    marketmapping: fieldValues.marketmapping,
                    dealerInfo: fieldValues.dealerInfo,
                    businessSplit: fieldValues.businessSplit,
                    storeArea: fieldValues.storeArea
                },
                //data: angular.toJson(listData)
            }).success(callback);


        }
    }
})