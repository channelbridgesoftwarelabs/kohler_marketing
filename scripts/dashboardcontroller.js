KohlerMarketModule.controller('DashboardController', function ($scope, $idle, $routeParams, $modal, ApplicationScopes, $location, WebAPIurl, DashboardService, AuthService, Portal) {
    $scope.cities = []
    $scope.catchments = []

    function isFloat(n) {
        return Number(n) === n && n % 1 !== 0;
    }

    $scope.countByBrandGridOptions = {
        columnDefs: [{name: 'brand', displayName: 'Brand Name'}, {
            name: 'value',
            displayName: 'Dealer Count'
        }, {
            name: 'numericReach',
            displayName: 'Numeric Reach (%)'
        }],
        data: []
    }

    $scope.marketShareByBrandGridOptions = {
        columnDefs: [{name: 'brand', displayName: 'Brand Name'}, {name: 'value', displayName: 'Market Share (%)'}],
        data: []
    }


    $scope.reachByBrandGridOptions = {
        columnDefs: [{name: 'brand', displayName: 'Brand Name'}, {
            name: 'value',
            displayName: 'Weighted Reach (%)'
        }],
        data: []
    }


    AuthService.validate()

    $scope.$on('$userIdle', function () {
        window.location.href = Portal
    });

    const GET_METHOD = {
        CITIES: 'GetCities',
        ZONES: 'GetZones',
        REGION: 'GetRegions',
        CATCHMENTS: 'GetCatchments',
        PROGRESS: 'GetProgress',
        DEALERS_BY_BRAND: 'GetDealersByBrand'
    }
    const SCOPE_VARIABLES = {
        CITIES: 'cities',
        ZONES: 'zones',
        REGION: 'regions',
        CATCHMENTS: 'catchments',
        TOTAL_PROGRESS_VALUE: 'total',
        TOTAL_PROGRESS_COMPLETED: 'completed',
        BRAND: 'brand',
        BRAND_VALUE: 'value',

    }
    const ARRAY_VALUES = {
        CITIES: 'city',
        REGION: 'region',
        ZONE: 'zone',
        CATCHMENTS: 'catchment',
        TOTAL_PROGRESS_VALUE: 'total',
        TOTAL_PROGRESS_COMPLETED: 'completed',
        BRAND: 'brand',
        BRAND_VALUE: 'value',

    }
    const DOCUMENT_ID = {
        CITIES: '#cities',
        REGION: '#regions',
        ZONES: '#zones',
        CATCHMENTS: '#catchments',
        PROGRESS_BAR: '#progressbar',
        DEALERS_BY_BRAND: '#dealersByBrand'
    }

    var init = function (city = [], region = [], catchment = [], zone = []) {
        LoadZones();
        LoadCities();
        LoadRegions();
        LoadCatchments();
        LoadProgress(city.join(','), region.join(','), catchment.join(','), zone.join(','));
        LoadDealersByBrand(city.join(','), region.join(','), catchment.join(','), zone.join(','));
        LoadMarketSharedByBrand(city.join(','), region.join(','), catchment.join(','), zone.join(','));
        LoadReachByBrand(city.join(','), region.join(','), catchment.join(','), zone.join(','))

    }

    $scope.init = init

    var LoadData = function (getMethod, scopeVariables, id, value) {
        DashboardService[getMethod](function (results) {
            if (getMethod == GET_METHOD.CITIES)
                results = [{city: 'All'}].concat(results)

            else if (getMethod == GET_METHOD.REGION)
                results = [{region: 'All'}].concat(results)

            else if (getMethod == GET_METHOD.CATCHMENTS) {
                console.log('inside catchment')
                console.log(results)
                console.log($scope[scopeVariables])
                results = [{catchment: 'All'}].concat(results)
            }

            else if (getMethod == GET_METHOD.ZONES) {
                results = [{zone: 'All'}].concat(results)
            }

            $scope[scopeVariables] = results.map(item => item[value]);
        }, $(id).val(), WebAPIurl);

    }

    // var LoadDataWithArguments = function (getMethod, scopeVariables1, scopeVariables2, id, value1, value2, arg1, arg2) {
    //     console.log(getMethod, scopeVariables1, scopeVariables2, id, value1, value2, arg1, arg2)
    //     DashboardService[getMethod](arg1, arg2, function (results) {
    //         if (Array.isArray(results)) {
    //             console.log('True')
    //             console.log(results)
    //             $scope[scopeVariables1] = results.map(item => item[value1]);
    //             $scope[scopeVariables2] = results.map(item => item[value2]);
    //         } else {
    //             console.log('false')
    //             console.log(results)
    //             $scope[scopeVariables1] = results[value1]
    //             $scope[scopeVariables2] = results[value2]
    //         }
    //
    //     }, $(id).val(), WebAPIurl);
    //
    // }

    var LoadCities = function () {
        LoadData(GET_METHOD.CITIES, SCOPE_VARIABLES.CITIES, DOCUMENT_ID.CITIES, ARRAY_VALUES.CITIES)
    }
    var LoadZones = function () {
        LoadData(GET_METHOD.ZONES, SCOPE_VARIABLES.ZONES, DOCUMENT_ID.ZONES, ARRAY_VALUES.ZONE)
    }

    var LoadRegions = function () {
        LoadData(GET_METHOD.REGION, SCOPE_VARIABLES.REGION, DOCUMENT_ID.REGION, ARRAY_VALUES.REGION)
    }

    var LoadCatchments = function () {
        LoadData(GET_METHOD.CATCHMENTS, SCOPE_VARIABLES.CATCHMENTS, DOCUMENT_ID.CATCHMENTS, ARRAY_VALUES.CATCHMENTS)
    }


    // var LoadProgress = function (city, region) {
    //     LoadDataWithArguments(GET_METHOD.PROGRESS, SCOPE_VARIABLES.TOTAL_PROGRESS_VALUE, SCOPE_VARIABLES.TOTAL_PROGRESS_COMPLETED, DOCUMENT_ID.PROGRESS_BAR, ARRAY_VALUES.TOTAL_PROGRESS_VALUE, ARRAY_VALUES.TOTAL_PROGRESS_COMPLETED, city, region)
    // }
    // var LoadDealersByBrand = function (city, region) {
    //     LoadDataWithArguments(GET_METHOD.DEALERS_BY_BRAND, SCOPE_VARIABLES.BRAND, SCOPE_VARIABLES.BRAND_VALUE, DOCUMENT_ID.DEALERS_BY_BRAND, ARRAY_VALUES.BRAND, ARRAY_VALUES.BRAND_VALUE, city, region)
    // }


    var LoadProgress = function (city, region, catchment, zone) {
        region = region == 'All' ? '' : region
        city = city == 'All' ? '' : city
        catchment = catchment == 'All' ? '' : catchment
        zone = zone == 'All' ? '' : zone

        DashboardService.GetProgress(city, region, catchment, zone, function (results) {
            $scope.total = results.total;
            $scope.completed = results.completed
        }, $('#progressbar').val(), WebAPIurl);


    }
    var LoadDealersByBrand = function (city, region, catchment, zone) {
        region = region == 'All' ? '' : region
        city = city == 'All' ? '' : city
        catchment = catchment == 'All' ? '' : catchment
        zone = zone == 'All' ? '' : zone
        DashboardService.GetDealersByBrand(city, region, catchment, zone, function (results) {
            $scope.countByBrandGridOptions.data = results.map(item => {
                var percentage = (item.value / item.totalDealers) * 100
                item.numericReach = isFloat(percentage) ? (percentage).toFixed(2) : percentage;
                return item;
            })
            $scope.labels = results.map(item => item.brand);
            $scope.dealersByBrand = results.map(item => item.value)
            $scope.countByBrandChartOptions = {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        }, $('#dealersByBrand').val(), WebAPIurl);


    }
    var LoadMarketSharedByBrand = function (city, region, catchment, zone) {
        region = region == 'All' ? '' : region
        city = city == 'All' ? '' : city
        catchment = catchment == 'All' ? '' : catchment
        zone = zone == 'All' ? '' : zone
        //Chart.defaults.global.legend = angular.extend({}, Chart.defaults.global.legend, {position: 'bottom', display: true})
        $scope.marketShareChartOptions = {
            legend: {
                position: 'bottom', display: true
            }
        }
        DashboardService.GetMarketSharedByBrand(city, region, catchment, zone, function (data) {
            var totalSales = data.map(item => item.value).reduce((acc, item) => acc + item, 0)
            var results = data.map(item => {
                var percentage = ((item.value / totalSales) * 100)
                item.value = isFloat(percentage) ? (percentage).toFixed(2) : percentage;
                return item;
            })
            $scope.labels = results.map(item => item.brand);
            $scope.marketShareByBrand = results.map(item => item.value)

            console.log('market share by brand')
            console.log(results)
            $scope.marketShareByBrandGridOptions.data = results
        }, $('#marketSharedByBrand').val(), WebAPIurl);


    }

    var LoadReachByBrand = function (city, region, catchment, zone) {
        region = region == 'All' ? '' : region
        city = city == 'All' ? '' : city
        catchment = catchment == 'All' ? '' : catchment
        zone = zone == 'All' ? '' : zone
        //Chart.defaults.global.legend = angular.extend({}, Chart.defaults.global.legend, {position: 'bottom', display: true})
        DashboardService.GetReachByBrand(city, region, catchment, zone, function (results) {
            console.log('weighted reach by brand')
            $scope.labels = results.map(item => item.brand);
            $scope.reachByBrand = results.map(item => item.value)
            $scope.reachByBrandGridOptions.data = results
            $scope.reachByBrandChartOptions = {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        }, $('#reachByBrand').val(), WebAPIurl);


    }

    init($scope.city, $scope.region, $scope.catchment, $scope.zone)


})