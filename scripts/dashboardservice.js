KohlerMarketModule.factory('DashboardService', function ($http, $routeParams, WebAPIurl) {
    var ko = $routeParams.ko.replace(/ko/ig, '')
    var baseURL = WebAPIurl
    return {
        getResource: function (callback, url) {
            $http.get(url).success(callback);
        },
        GetCities: function (callback) {
            this.getResource(callback, baseURL + 'cities?ko=' + ko)
        },
        GetRegions: function (callback) {
            this.getResource(callback, baseURL + 'regions?ko=' + ko)

        },
        GetZones: function (callback) {
            this.getResource(callback, baseURL + 'zones?ko=' + ko)

        },
        GetCatchments: function (callback) {
            this.getResource(callback, baseURL + 'catchments?ko=' + ko)
        },

        GetProgress: function (city, region, catchment, zone, callback) {
            this.getResource(callback, baseURL + 'progress?ko=' + ko + '&catchment=' + catchment + '&city=' + city + '&region=' + region + '&zone=' + zone)
        },
        GetDealersByBrand: function (city, region, catchment, zone, callback) {
            this.getResource(callback, baseURL + 'dealersByBrand?ko=' + ko + '&catchment=' + catchment + '&city=' + city + '&region=' + region + '&zone=' + zone)
        },
        GetMarketSharedByBrand: function (city, region, catchment, zone, callback) {
            this.getResource(callback, baseURL + 'marketShareByBrand?ko=' + ko + '&catchment=' + catchment + '&city=' + city + '&region=' + region + '&zone=' + zone)
        },

        GetReachByBrand: function (city, region, catchment, zone, callback) {
            this.getResource(callback, baseURL + 'weightedReachByBrand?ko=' + ko + '&catchment=' + catchment + '&city=' + city + '&region=' + region + '&zone=' + zone)
        },

        GetAssignedCatchments: function (callback) {
            this.getResource(callback, baseURL + 'assignedCatchmentsByKo?ko=' + ko)
        },


    }
});