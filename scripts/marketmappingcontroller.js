KohlerMarketModule.controller('MarketMappingController', function ($scope, $routeParams, $modal, ApplicationScopes, $location, marketmappingservice, WebAPIurl, marketmappingpreviewservice, AuthService) {


    $("#ddlDealerNameDuplicate").change(function () {
        console.log('value changes')
        console.log($(this).val())
    })

    AuthService.validate()


    $scope.isDuplicate = false

    $("#ddlDistributorForDiv").hide()

    $("#ddlStoreType").on("change", function (e) {
        if ($("#ddlStoreType").val() == 'Distributor and Retailing Sanitary' || $("#ddlStoreType").val() == 'Distributor but Not Retailing Sanitary') {
            $("#ddlDistributorForDiv").show()
        }
        else
            $("#ddlDistributorForDiv").hide()
    })


    $scope.marketmapping = {};
    $scope.dealerInfo = {};
    $scope.businessSplit = {};
    $scope.storeArea = {};
    $scope.dealerList = {};
    $scope.$watch("businessSplit", function (control) {
        //  debugger;
        var arr = [];
        arr.push(control);
        var businessSplitCount = 0;
        var annualSanitoryBiz = 0;
        for (var key in arr[0]) {
            if (arr[0][key] != undefined && arr[0][key] != "") {
                if (!isNaN(arr[0][key]))
                    annualSanitoryBiz = parseInt(annualSanitoryBiz) + parseInt(arr[0][key]);
                businessSplitCount++;
            }
        }
        if (arr[0] != null) {
            $scope.dealerInfo.txtCompetitorBrands = businessSplitCount;
            $scope.dealerInfo.txtAnnualSanitoryBiz = annualSanitoryBiz;
        }

    }, true);
    $scope.$watch("dealerInfo", function (control) {
        if (control != null && control != undefined) {
            $scope.dealerInfo.txtAnnualSanitoryProject = !isNaN(parseInt($scope.dealerInfo.txtAnnualSanitoryBiz) - parseInt($scope.dealerInfo.txtAnnualSanitoryRetail)) ? parseInt($scope.dealerInfo.txtAnnualSanitoryBiz) - parseInt($scope.dealerInfo.txtAnnualSanitoryRetail) : "";
            $scope.dealerInfo.txtAnnualCPBiz = !isNaN(parseInt($scope.dealerInfo.txtAnnualSanitoryBiz) - (parseInt($scope.dealerInfo.txtAnnualVCBiz) + parseInt($scope.dealerInfo.txtAnnualShowerEnclosureBiz))) ? parseInt($scope.dealerInfo.txtAnnualSanitoryBiz) - (parseInt($scope.dealerInfo.txtAnnualVCBiz) + parseInt($scope.dealerInfo.txtAnnualShowerEnclosureBiz)) : "";
        }

    }, true);


    $scope.LoadDealerDetails = function () {

        var savedScope = ApplicationScopes.get('stagedData');
        if (savedScope) {
            $scope.UpdateModel(savedScope);
        }

        marketmappingservice.GetDealerDetails(function (results) {
            $scope.UpdateModel(results);
            var mergedData = angular.extend({}, results, savedScope)
            if (savedScope) {
                $scope.UpdateModel(mergedData);
            }

        }, $routeParams.prospectId, WebAPIurl);


    }


    $scope.UpdateModel = function (results) {
        //$timeout(function(){
        $scope.marketmapping = {};
        $scope.dealerInfo = {};
        $scope.businessSplit = {};
        $scope.storeArea = {};
        $scope.marketmapping.prospectName = results.prospectName
        $scope.marketmapping.lblZone = results.zone;
        $scope.marketmapping.lblAddress = results.address;
        $scope.marketmapping.lblCity = results.city;
        $scope.marketmapping.lblCityRank = results.cityRank;
        $scope.marketmapping.lblDealerSource = results.sourceCompanyName;
        $scope.marketmapping.lblTerritory = results.territory;
        $scope.marketmapping.lblDealerRefNo = results.prospectId;
        $scope.marketmapping.lblState = results.state;
        $scope.marketmapping.lblRegion = results.region;
        $scope.marketmapping.txtContactName = results.contact_person_name;
        $scope.marketmapping.txtContactEmail = results.email;
        // $scope.marketmapping.lblArea = results.region;
        $scope.marketmapping.lblASMName = results.asm;
        $scope.marketmapping.lblCatchmentName = results.catchmentName;
        $scope.marketmapping.txtContactNo = results.contact_number;
        $scope.marketmapping.lblState = results.state;

        if (results.dealer_info) {
            var dealerinfo = JSON.parse(results.dealer_info)
            $scope.dealerInfo = dealerinfo;
        }
        if (results.business_split) {
            var businesssplit = JSON.parse(results.business_split)
            $scope.businessSplit = businesssplit;
        }
        if (results.store_area) {
            var storearea = JSON.parse(results.store_area)
            $scope.storeArea = storearea;
        }
        if ($scope.dealerInfo != null) {
            var ddlDealerFor = $scope.dealerInfo.ddlDealerFor
            var ddlDistributorFor = $scope.dealerInfo.ddlDistributorFor
            $("#ddlStoreLocation").multipleSelect('setSelects', [$scope.dealerInfo.ddlStoreLocation]);
            $("#ddlStoreType").multipleSelect('setSelects', [$scope.dealerInfo.ddlStoreType]);
            $("#ddlDealerFor").multipleSelect('setSelects', ddlDealerFor ? ddlDealerFor.split(',') : ''); //[$scope.dealerInfo.ddlDealerFor]);
            $("#ddlDistributorFor").multipleSelect('setSelects', ddlDistributorFor ? ddlDistributorFor.split(',') : '');
            //debugger;
            if ($scope.dealerInfo.ddlDealerFor == "Others")
                $('#txtDealerForOther').show();
            if ($scope.dealerInfo.ddlDistributorFor == "Others")
                $('#txtDistributorForOther').show();

            $('#ddlDealerNameDuplicate').val($scope.dealerInfo.ddlDealerNameDuplicate);

            if ($scope.dealerInfo.ddlDealerNameDuplicate != undefined && $scope.dealerInfo.ddlDealerNameDuplicate != "")
                $('#chkDuplicate')[0].checked = true;
            else
                $('#chkDuplicate')[0].checked = false;
        }
        else {
            $('#ddlDealerNameDuplicate').val('');
            $('#chkDuplicate')[0].checked = false;
        }
        $('#ddlDealerNameDuplicate').inputpicker('destroy');
        $('#ddlDealerNameDuplicate').inputpicker({
            data: $scope.dealerList.filter(item => item.status == 'Completed'),
            fields: ["prospectName", "status", "catchmentName", "sourceCompanyName", "prospectId"],
            headShow: true,
            fieldText: 'prospectName',
            fieldValue: 'prospectId',
            filterOpen: true,
            filterField: 'prospectName',
            rowSelectedBackgroundColor: '#248cb5',

        });
    }


    $scope.LoadPreview = function (isDuplicate) {

        console.log("Inside LoadPreview")

        //debugger;
        if (!isDuplicate) {
            var storeLocation = $("#ddlStoreLocation").multipleSelect('getSelects', 'value')[0];
            if (storeLocation == undefined || storeLocation == null || storeLocation == "") {
                alert("Please select store location");
                return false;
            }
            if (!$scope.ValidateBusinessSplit()) {
                alert("Please enter atleast one Business Split value");
                return false;
            }
            if (!$scope.ValidateStoreArea()) {
                alert("Please enter atleast one Store Area value");
                return false;
            }

            if ($scope.storeArea.txtSAStoreSize != null && $scope.storeArea.txtSAStoreSize != "" && $scope.storeArea.txtSAStoreSize < 100) {
                alert("Store size cannot be less than 100 Sq.ft");
                return false;
            }
        }

        $scope.AddDropDownFields();
        ApplicationScopes.store('MarketMappingController', $scope);
        ApplicationScopes.store('stagedData', {
            dealer_info: JSON.stringify($scope.dealerInfo),
            business_split: JSON.stringify($scope.businessSplit),
            store_area: JSON.stringify($scope.storeArea)
        });
        $location.path("/preview/" + $routeParams.token + "/" + $routeParams.ko + "/" + $routeParams.prospectId);
    }


    $scope.AddDropDownFields = function () {
        var storeLocation = $("#ddlStoreLocation").multipleSelect('getSelects', 'value')[0];
        $scope.dealerInfo.ddlStoreLocation = storeLocation;
        $scope.dealerInfo.ddlStoreType = $("#ddlStoreType").multipleSelect('getSelects', 'value')[0];
        // debugger;
        //  if ($scope.dealerInfo.txtDealerFor == undefined || $scope.dealerInfo.txtDealerFor == "")

        if ($('#ddlDealerFor').multipleSelect('getSelects', 'value').toString() != "") {
            var dealerFor = "";
            for (var itemCnt = 0; itemCnt < $('#ddlDealerFor').multipleSelect('getSelects', 'value').toString().split(',').length; itemCnt++) {
                dealerFor += $('#ddlDealerFor').multipleSelect('getSelects', 'value').toString().split(',')[itemCnt] + ",";
            }
            $scope.dealerInfo.ddlDealerFor = dealerFor.substring(0, dealerFor.length - 1);
        }
        if ($('#ddlDistributorFor').multipleSelect('getSelects', 'value').toString() != "") {
            var distributorFor = "";
            for (var itemCnt = 0; itemCnt < $('#ddlDistributorFor').multipleSelect('getSelects', 'value').toString().split(',').length; itemCnt++) {
                distributorFor += $('#ddlDistributorFor').multipleSelect('getSelects', 'value').toString().split(',')[itemCnt] + ",";
            }
            $scope.dealerInfo.ddlDistributorFor = distributorFor.substring(0, distributorFor.length - 1);
        }

        if ($('#chkDuplicate')[0].checked)
            $scope.dealerInfo.ddlDealerNameDuplicate = $('#ddlDealerNameDuplicate').val();
    }

    $scope.ValidateBusinessSplit = function () {
        var ValueFilled = false;
        $("#divBusinessSplit input").each(function () {
            // debugger;
            console.log(this.value)
            if (this.value != null && this.value != "") {
                ValueFilled = true;
                return false;
            }

        });

        return ValueFilled;
    }


    $scope.ValidateStoreArea = function () {
        var ValueFilled = false;
        $("#divStoreArea input").each(function () {
            if (this.value != null && this.value != "") {
                ValueFilled = true;
                return false;
            }

        });

        return ValueFilled;
    }

    $scope.SaveData = function () {
        $scope.AddDropDownFields();
        marketmappingpreviewservice.SubmitData(function (results) {
            alert("Data Saved successfully");
        }, WebAPIurl, $scope, $routeParams.ko, $('#ddlDealerName').val());
    }


    if ($routeParams.prospectId != null) {
        $scope.dealerList = ApplicationScopes.get('DealerDetailsController').dealerList;
        $('#ddlDealerName').val($routeParams.prospectId);
        //     $('#ddlDealerName').inputpicker({
        //         data: angular.copy($scope.dealerList),
        //         fields: ["prospectName", "status", "catchmentName", "sourceCompanyName", "prospectId"],
        //         headShow: true,
        //         fieldText: 'prospectName',
        //         fieldValue: 'prospectId',
        //         filterOpen: true,
        //         filterField: 'text',
        //         rowSelectedFontColor: '#ffffff',
        //         rowSelectedBackgroundColor: '#C19A5A'
        //         //rowSelectedBackgroundColor: '#248cb5',
        //
        // });


        $scope.LoadDealerDetails();
    }
})

