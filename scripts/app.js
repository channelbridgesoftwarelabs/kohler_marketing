angular.module('Kohler.config', [])
    .constant('WebAPIurl', 'http://kohler-incent.cburls.com:9090/mapi/')
    .constant('Portal', 'http://partnersint.kohler.co.in/distpo/')
    //.constant('Portal', 'http://52.66.15.216:8080/distpo/')
//    .constant('WebAPIurl', 'http://localhost:9000/mapi/');


var KohlerMarketModule = angular.module('KohlerMarketapp', ['$idle', 'ngRoute', 'chart.js', 'ui.bootstrap', 'Kohler.config', 'ui.grid', 'ui.grid.selection', 'ui.grid.resizeColumns', 'ui.grid.moveColumns', 'ui.grid.pagination', 'ui.grid.autoResize', 'ui.grid.pinning','ui.grid.exporter']);
KohlerMarketModule.config(['$routeProvider', '$idleProvider',
    function ($routeProvider, $idleProvider) {
        var MarketMappingPreview = './MarketMappingPreview.html?' + Date.now()
        var MarketMapping = './MarketMapping.html?' + Date.now()
        var DealerDetails = './DealerDetails.html?' + Date.now()
        var Dashboard = './Dashboard.html?' + Date.now()
        $routeProvider
            .when('/preview/:token/:ko/:prospectId', {templateUrl: MarketMappingPreview})
            //.when('/landing', {templateUrl: DealerDetails})
            .when('/landing/:token/:ko', {templateUrl: DealerDetails})
            .when('/landing/:token/:ko/:prospectId', {templateUrl: MarketMapping})
            .when('/dashboard/:token/:ko', {templateUrl: Dashboard})
            .otherwise({redirectTo: '/landing/:token/:ko'});
        $idleProvider.setIdleTime(15 * 60);
        // User will timeout at the end of 15 seconds anfter considered idle.
        $idleProvider.setTimeoutTime(10);
    }]);

KohlerMarketModule.run([
    '$rootScope', '$routeParams', '$location', '$idle', 'Portal', '$route', '$location',
    function ($rootScope, $routeParams, $location, $idle, Portal, $route, $location) {

        $idle.watch();


        $rootScope.$routeParams = $routeParams
        $routeParams.$location = $location
        $rootScope.$route = $route
        $rootScope.$location = $location
        $rootScope.Portal = Portal
        // see what's going on when the route tries to change
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            // next is an object that is the route that we are starting to go to
            // current is an object that is the route where we are currently
            // if (typeof (current) !== 'undefined') {
            //$templateCache.remove(current.templateUrl);
            // }
            var currentPath;
            if (current === undefined) {
                currentPath = "";
            } else {
                currentPath = current.originalPath;
            }
            var nextPath = next.originalPath;

            console.log('Changing route from %s to %s', currentPath, nextPath);
        });


        //$rootScope.$on('scope.stored', function (event, data) {
        //    console.log("scope.stored", data);
        //});
    }
]);

KohlerMarketModule.factory('ApplicationScopes', function ($rootScope) {
    var mem = {};

    return {
        store: function (key, value) {
            $rootScope.$emit('scope.stored', key);
            mem[key] = value;
        },
        get: function (key) {
            return mem[key];
        }
    };
});


KohlerMarketModule.service('AuthService', function ($rootScope, $routeParams, Portal, $http) {
    return {
        validate: function () {
            // $http.get(Portal + 'mapi/isValidExternalSession.jpg?token=' + $routeParams.token)
            //     .then(function (resp) {
            //         console.log('validating token')
            //         var data = resp.data
            //         if (!data.status) {
            //             window.location.href = Portal
            //         }
            //     })
        }
    }
})


