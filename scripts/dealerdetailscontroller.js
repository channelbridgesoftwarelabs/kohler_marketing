KohlerMarketModule.controller('DealerDetailsController', function (uiGridConstants, $modal, $scope, $idle, $routeParams, ApplicationScopes, $location, marketmappingservice, WebAPIurl, Portal, $rootScope, AuthService, DashboardService) {
    $rootScope.$routeParams = $routeParams
    $scope.dealerList = {};
    $scope.gridOptions = {};
    $scope.catchments = []
    $scope.gridOptions.enableGridMenu = true;
    $scope.gridOptions.enableSelectAll = false;
    $scope.gridOptions.multiSelect = false;
    $scope.gridOptions.modifierKeysToMultiSelect = false;
    $scope.gridOptions.noUnselect = false;
    $scope.gridOptions.enableFiltering = true;
    $scope.gridOptions.enablePaginationControls = true;
    $scope.gridOptions.enableExporting = true;

    $scope.gridOptions.paginationPageSizes = [100, 200, 300];
    $scope.gridOptions.paginationPageSize = 100;
    $scope.gridOptions.enableColumnResizing = true;
    $scope.loadingData = false;

    var ko = $routeParams.ko.replace(/ko/ig, '')
    $scope.userKo = $routeParams.ko.replace(/ko/ig, '')
    var token = $routeParams.token
    console.log('ko =' + ko)

    var loadDealers = function () {
        $scope.loadingData = true;
        marketmappingservice.GetDealers(function (results) {
            $scope.dealerList = results;
            $scope.duplicateDealerList = results.filter(item => item.status == 'Completed')
            ApplicationScopes.store('DealerDetailsController', $scope);
            $scope.gridOptions.data = results;
            $scope.loadingData = false;

        }, $routeParams.ko.replace(/ko/ig, ''), WebAPIurl);

    }

    var loadCatchments = function () {
        DashboardService.GetAssignedCatchments(function (results) {
            $scope.catchments = results
        }, $routeParams.ko.replace(/ko/ig, ''), WebAPIurl);
    }

    AuthService.validate()

    loadDealers()
    loadCatchments()

    $scope.addNew = function () {
        alertModalInstance = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'confirmAlert.html',
            scope: $scope
        });

    }

    $scope.cancelDelete = function () {
        alertModalInstance.dismiss('cancel');
    };

    $scope.dealer = {}

    $scope.ok = function (dealer) {
        alertModalInstance.close(true);
        marketmappingservice.addNewDealer(function () {
            alert('New Dealer Added Successfully!')
            loadDealers()
        }, Object.assign({}, dealer, {ko: ko}), WebAPIurl)

    };


    console.log('dealer list controller initialized')
    $scope.$on('$userIdle', function () {
        window.location.href = Portal
    });

    $scope.gridOptions.columnDefs = [
        {
            name: 'prospectName',
            displayName: 'Prospect Name',
            width: '30%',
            cellTemplate: '<div class="ui-grid-cell-contents">' + '<a href="#/landing/' + token + '/' + ko + '/{{row.entity.prospectId}}">{{row.entity.prospectName}}</a>' + '</div>',
            enablePinning: true,
            hidePinLeft: false,
            hidePinRight: true,
            pinnedLeft: true
        },
        //{ name: 'prospectName', displayName: 'Prospect Name', width: '30%', enablePinning: true, hidePinLeft: false, hidePinRight: true,pinnedLeft:true  },
        {name: 'status', displayName: 'Status', width: '10%'},
        {name: 'region', displayName: 'Region', width: '10%'},
        {name: 'zone', displayName: 'Zone', width: '15%'},
        {name: 'city', displayName: 'City', width: '10%'},
        {name: 'territory', displayName: 'Territory', width: '15%'},
        {name: 'catchmentName', displayName: 'Catchment Name', width: '20%'},
        {name: 'sourceCompanyName', displayName: 'Source Company Name', width: '10%'},
        {name: 'prospectId', displayName: 'Prospect Id', width: '11%'},
        //{name: 'prospectCode', displayName: 'Prospect Code', width: '10%'},
        {name: 'cityRank', displayName: 'City Rank', width: '10%'},


    ];


    // $scope.LoadOnce = function () {


    $scope.gridOptions.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;
        // gridApi.selection.on.rowSelectionChanged($scope, function (row) {
        //     $location.path("/landing/" + $routeParams.ko.replace(/ko/ig, '') + "/" + row.entity.prospectId);
        // });
    }


});

