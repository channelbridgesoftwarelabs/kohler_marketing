﻿// Creating a factory from the module

KohlerMarketModule.factory('marketmappingservice', function ($http) {
    //debugger;
    return {
        GetDealers: function (callback, ko, WebAPIurl) {

            var url = WebAPIurl + 'prospects?ko=' + ko;
            //   var url = "http://kohler-incent.cburls.com:9090/mapi/prospects?ko="+ko;
            $http.get(url).success(callback);
        },

        addNewDealer: function (callback, prospect, WebAPIurl) {
            var url = WebAPIurl + 'newProspect';
            $http.post(url, prospect).success(callback)
        },
        GetDealerDetails: function (callback, prospectId, WebAPIurl) {

            var url = WebAPIurl + 'prospectDetail?prospectId=' + prospectId;
            console.log(url)
            //   var url = "http://kohler-incent.cburls.com:9090/mapi/prospectDetail?prospectId=" + prospectId;
            $http.get(url).success(callback);
        },

    }
})